## Introduzione

Sistema basato su Docker4WordPress.
Full documentation is available at https://wodby.com/docs/stacks/wordpress/local.

## Stack

The WordPress stack consist of the following containers:

| Container       | Versions           | Service name    | Image                              | Default |
| -------------   | ------------------ | ------------    | ---------------------------------- | ------- |
| [Nginx]         | 1.15, 1.14         | `nginx`         | [wodby/nginx]                      | ✓       |
| [WordPress]     | 4                  | `php`           | [wodby/wordpress]                  | ✓       |
| [PHP]           | 7.2, 7.1, 5.6      | `php`           | [wodby/wordpress-php]              | ✓       |
| [MariaDB]       | 10.3, 10.2, 10.1   | `mariadb`       | [wodby/mariadb]                    | ✓       |
| [Mailhog]       | latest             | `mailhog`       | [mailhog/mailhog]                  | ✓       |
| phpMyAdmin      | latest             | `pma`           | [phpmyadmin/phpmyadmin]            | ✓       |
| Portainer       | latest             | `portainer`     | [portainer/portainer]              | ✓       |
| Traefik         | latest             | `traefik`       | [_/traefik]                        | ✓       |

Supported WordPress versions: 4

## Avvio instanza

- Attivare docker
- Eseguire il comando: docker-compose up -d
- Eseguire il comando: docker-compose -f traefik.yml up -d
- Eseguire il comando: sudo nano /etc/hosts
    127.0.0.1 www.carpanelli.com
    127.0.0.1 portainer.www.carpanelli.com
    127.0.0.1 pma.www.carpanelli.com
    127.0.0.1 mailhog.www.carpanelli.com

## Tools

http://www.carpanelli.com:8080/dashboard/ - Traefik
http://portainer.www.carpanelli.com/ - Docker / LOG / SSH images
http://mailhog.www.carpanelli.com/ - Log invio email
http://pma.www.carpanelli.com/ - DB

## SITE

http://www.carpanelli.com/ - Site

http://www.carpanelli.com/wp-login.php
admin
admin123

## SET DB
wp-config.php
## SET SYSTEM
.env
